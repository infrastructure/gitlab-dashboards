#!/usr/bin/python3
# SPDX-License-Identifier: MIT

import gitlab

from jinja2 import Environment, FileSystemLoader
import os

if os.environ.get("CI_SERVER"):
    token = os.environ["DASHBOARD_ACCESS_TOKEN"]
    gl = gitlab.Gitlab("http://" + os.environ["CI_SERVER_HOST"], private_token=token)
else:
    gl = gitlab.Gitlab.from_config('apertis')

gl.auth()

class Schedule:
    def __init__ (self, distro, last_url, status):
        self.distro = distro
        self.last_url = last_url
        self.status = status

class Package:
    def __init__(self, name, component, url):
        self.name = name
        self.schedules = {}
        self.url = url

def introspect_package (name, component, gl_project):
    p = Package(name, component, gl_project.web_url)

    for s in  gl_project.pipelineschedules.list(as_list=False, lazy=True):
        # The pipeline schedule object returned as list doesn't have all data
        # in particular last_pipeline
        sched = gl_project.pipelineschedules.get(s.id)
        source_distro = sched.ref.split("/")[1].split("-")[0]
        url = None
        status = None

        if sched.last_pipeline:
            url = sched.last_pipeline["web_url"]
            status = sched.last_pipeline["status"]

        p.schedules[source_distro] = Schedule(source_distro, url, status)

    return p

def introspect_component (component):
    group = gl.groups.get(f"pkg/{component}", lazy=True)
    packages = []

    for gp in group.projects.list(as_list =False, order_by="name", sort="asc"):
        p = gl.projects.get(gp.id)

        packages.append(introspect_package (p.name, component, p))
    return packages


def generate_distro_component_page(distro, component, packages):
    env = Environment(loader=FileSystemLoader('templates/'))
    data = env.get_template('debian-updates.html.jinja2').render(
        distro=distro, component=component, packages=packages)

    print(f"Generating test case page {distro} {component}")
    with open(os.path.join(f"{distro}-{component}-debian-updates.html"), 'w') as updates_file:
        updates_file.write(data)

def generate_index_file(distros, components):
    env = Environment(loader=FileSystemLoader('templates/'))
    data = env.get_template('index.html.jinja2').render(
        distros=distros, components=components)

    print(f"Generating index page")
    with open(os.path.join(f"index.html"), 'w') as index_file:
        index_file.write(data)


components = ["target" , "development", "sdk"]
source_distros = ["buster" ]

for c in components:
    packages = introspect_component (c)
    for d in source_distros:
        generate_distro_component_page (d, c, packages)

generate_index_file(source_distros, components)
